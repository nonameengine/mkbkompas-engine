/*
*  File: platform.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 12-11-2017
*
*  Copyright (c) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include "types.h"

namespace Engine
{
    enum FileOrigin
    {
        FILE_ORIGIN_BEGIN,
        FILE_ORIGIN_CURRENT,
        FILE_ORIGIN_END
    };

    using FileHandle = size_t;

    const FileHandle INVALID_FILE_HANDLE = -1;
    const size_t INVALID_FILE_POINTER = -1;
    
    class Platform
    {
    public:
        virtual ~Platform() {}
        
        virtual bool CreateMainWindow() = 0;
        virtual void DestroyMainWindow() = 0;
        virtual void SetMainWindowSize(int width, int height, bool setFullscreen) = 0;
        virtual bool UpdateMainWindow() = 0;

        virtual FileHandle OpenFileRead(const String &path) = 0;
        virtual FileHandle OpenFileWrite(const String &path) = 0;
        virtual size_t GetFilePointer(FileHandle handle, FileOrigin origin) = 0;
        virtual size_t SetFilePointer(FileHandle handle, FileOrigin origin, size_t offset) = 0;
        virtual bool ReadFile(FileHandle handle, Byte *data, size_t size) = 0;
        virtual bool WriteFile(FileHandle handle, const Byte *data, size_t size) = 0;
        virtual bool IsEof(FileHandle handle) = 0;
        virtual size_t GetFileSize(FileHandle handle) = 0;
        virtual void CloseFile(FileHandle handle) = 0;
    };
    
    Platform *GetPlatform();
}