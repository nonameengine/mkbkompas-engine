/*
*  File: filesystem.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 06-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include <stdint.h>

#include <memory>

#include "types.h"

namespace Engine
{
    class File
    {
    public:
        virtual ~File()
        {
        }

        inline int8_t ReadInt8() const
        {
            int8_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline uint8_t ReadUInt8() const
        {
            uint8_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline int16_t ReadInt16() const
        {
            int16_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline uint16_t ReadUInt16() const
        {
            uint16_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline int32_t ReadInt32() const
        {
            int32_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline uint32_t ReadUInt32() const
        {
            uint32_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline int64_t ReadInt64() const
        {
            int64_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline uint64_t ReadUInt64() const
        {
            uint64_t value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline float ReadFloat() const
        {
            float value;
            Read(reinterpret_cast<Byte*>(&value), sizeof(value));
            return value;
        }

        inline String ReadString() const
        {
            String value;
            const int32_t length = ReadInt32();
            value.resize(length);
            Read(reinterpret_cast<Byte*>(&value[0]), length);
            return value;
        }

        inline String ReadZeroTerminatedString() const
        {
            String value;
            for (;;) {
                const char letter = ReadInt8();
                if (letter == '\0')
                    break;

                value += letter;
            }
            return value;
        }

        inline void WriteInt8(const int8_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteUInt8(const uint8_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteInt16(const int16_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteUInt16(const uint16_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteInt32(const int32_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteUInt32(const uint32_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteInt64(const int64_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteUInt64(const uint64_t value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteFloat(const float value)
        {
            Write(reinterpret_cast<const Byte*>(&value), sizeof(value));
        }

        inline void WriteString(const String &value)
        {
            WriteUInt16(static_cast<uint16_t>(value.size()));
            Write(reinterpret_cast<const Byte*>(&value[0]), value.size());
        }

        inline void WriteZeroTerminatedString(const String &value)
        {
            const char zeroCharacter = '\0';
            Write(reinterpret_cast<const Byte*>(&value[0]), value.size());
            WriteInt8(zeroCharacter);
        }

        virtual void Read(Byte *data, int size) const= 0;
        virtual void Write(const Byte *data, int size) = 0;
        virtual bool IsEof() const = 0;
        virtual size_t GetSize() const = 0;
        virtual void Skip(size_t nbytes) = 0;
    };

    using FilePtr = std::shared_ptr<File>;

    class FileSystem
    {
    public:
        virtual ~FileSystem() {}

        virtual FilePtr OpenFileRead(const String &path) = 0;
        virtual FilePtr OpenFileWrite(const String &path) = 0;
    };

    FileSystem *GetFileSystem();
}
