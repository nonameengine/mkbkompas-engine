/*
*  File: model.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 09-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include <memory>
#include <vector>

#include "math.h"
#include "renderer.h"

namespace Engine
{
    struct GraphicsShape
    {
        HardwareBufferId hardwareBufferId;
    };

    using GraphicsShapePtr = std::shared_ptr<GraphicsShape>;
    using GraphicsShapeList = std::vector<GraphicsShapePtr>;

    class Model
    {
    public:
        virtual ~Model() {}

        virtual void Load() = 0;
        virtual void Unload() = 0;
        virtual bool IsLoaded() const = 0;

        virtual GraphicsShapeList GetGraphicsShapes() const = 0;
    };

    using ModelPtr = std::shared_ptr<Model>;

    class ModelManager
    {
    public:
        virtual ~ModelManager() {}

        virtual ModelPtr GetModel(const String &modelName) = 0;
    };

    ModelManager *GetModelManager();
}