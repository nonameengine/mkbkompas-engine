/*
*  File: platform.cpp
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 12-11-2017
*
*  Copyright (c) 2017 Ivan Shishkin All rights reserved
*/

#include <Windows.h>

#include <array>
#include <algorithm>

#include "platform.h"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")

namespace Engine
{
    const char MAIN_WINDOW_CLASS[] = "MAIN_WINDOW_CLASS";
    const char MAIN_WINDOW_TITLE[] = "Game";

    const int MAIN_WINDOW_WIDTH = 320;
    const int MAIN_WINDOW_HEIGHT = 240;

    const int MAIN_WINDOW_STYLE = WS_CAPTION | WS_SYSMENU | WS_BORDER | WS_VISIBLE;

    static class PlatformWin32 *platform;

    static constexpr std::array<std::pair<FileOrigin, DWORD>, 3> fileOriginTab = {
        std::make_pair(FILE_ORIGIN_BEGIN, FILE_BEGIN),
        std::make_pair(FILE_ORIGIN_CURRENT, FILE_CURRENT),
        std::make_pair(FILE_ORIGIN_END, FILE_END)
    };

    static DWORD ConvertToPlatformFileOrigin(FileOrigin origin)
    {
        const auto it = std::find_if(
            fileOriginTab.begin(),
            fileOriginTab.end(),
            [origin](const std::pair<FileOrigin, DWORD> &pair) {
                return (pair.first == origin);
            }
        );
        return it->second;
    }

    static WString ConvertToUnicode(const String &source)
    {
        int size = MultiByteToWideChar(CP_UTF8, 0, source.data(), source.size(), NULL, 0);
        if (!size)
            return WString();

        WString target;
        target.resize(size);
        size = MultiByteToWideChar(CP_UTF8, 0, source.data(), source.size(), &target[0], target.size());
        if (!size)
            return WString();

        return target;
    }

    static String ConvertFromUnicode(const WString &source)
    {
        int size = WideCharToMultiByte(CP_UTF8, 0, source.data(), source.size(), NULL, 0, NULL, NULL);
        if (!size)
            return String();

        String target;
        target.resize(size);
        size = WideCharToMultiByte(CP_UTF8, 0, source.data(), source.size(), &target[0], target.size(), NULL, NULL);
        if (!size)
            return String();

        return target;
    }

    class PlatformWin32 : public Platform
    {
    private:
        HWND mainWindow;
        HGLRC renderContext;
        bool fullscreen;

    private:
        static LRESULT CALLBACK WindowProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
        {
            const int SUCCESS = 0;
            const int FAIL = -1;

            switch (umsg) {
            case WM_CREATE:
                if (!platform->InitializeMainWindow(hwnd))
                    return FAIL;

                return SUCCESS;

            case WM_DESTROY:
                platform->DeinitializeMainWindow();
                return SUCCESS;

            case WM_ACTIVATE:
                if (platform->fullscreen) {
                    if (LOWORD(wparam) == WA_INACTIVE) {
                        if (!platform->MinimizeMainWindow())
                            return FAIL;
                    }
                    else {
                        if (!platform->RestoreMainWindow())
                            return FAIL;
                    }
                }
                return SUCCESS;
            }

            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }

        bool InitializeMainWindow(HWND hwnd)
        {
            PIXELFORMATDESCRIPTOR pfd{};

            pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
            pfd.nVersion = 1;
            pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
            pfd.iPixelType = PFD_TYPE_RGBA;
            pfd.cColorBits = 32;
            pfd.cDepthBits = 24;
            pfd.cStencilBits = 8;
            pfd.cAuxBuffers = 0;
            pfd.iLayerType = PFD_MAIN_PLANE;

            if (const HDC deviceContext = GetDC(hwnd)) {
                if (const int pixelFormat = ChoosePixelFormat(deviceContext, &pfd)) {
                    if (SetPixelFormat(deviceContext, pixelFormat, &pfd) == FALSE)
                        return false;

                    const HGLRC hglrc = wglCreateContext(deviceContext);
                    if (!hglrc)
                        return false;

                    if (wglMakeCurrent(deviceContext, hglrc) == TRUE) {
                        mainWindow = hwnd;
                        renderContext = hglrc;
                        return true;
                    }
                }
            }

            return false;
        }

        void DeinitializeMainWindow()
        {
            if (HDC deviceContext = GetDC(mainWindow))
                wglMakeCurrent(deviceContext, 0);

            wglDeleteContext(renderContext);
            renderContext = NULL;
            mainWindow = NULL;
        }

        bool SetDisplayFullscreenMode(int width, int height, bool enableFullscreen)
        {
            if (enableFullscreen) {
                DEVMODE displaySettings{};
                displaySettings.dmSize = sizeof(DEVMODE);
                displaySettings.dmPelsWidth = width;
                displaySettings.dmPelsHeight = height;
                displaySettings.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;

                if (LONG result = ChangeDisplaySettings(&displaySettings, fullscreen ? 0 : CDS_FULLSCREEN)) {
                    //TODO: log warning
                    return false;
                }

                InvalidateRect(mainWindow, 0, true);
            }
            else {
                if (ChangeDisplaySettings(0, 0) != DISP_CHANGE_SUCCESSFUL) {
                    return false;
                }
            }

            ShowCursor(!enableFullscreen);
            return true;
        }

        bool MinimizeMainWindow()
        {
            if (fullscreen) {
                if (!SetDisplayFullscreenMode(0, 0, false))
                    return false;
            }

            if (ShowWindow(platform->mainWindow, SW_MINIMIZE) == FALSE)
                return false;

            return true;
        }

        bool RestoreMainWindow()
        {
            if (fullscreen) {
                RECT windowRect;
                if (GetWindowRect(mainWindow, &windowRect) == FALSE)
                    return false;

                const int width = windowRect.right - windowRect.left;
                const int height = windowRect.bottom - windowRect.top;
                if (!SetDisplayFullscreenMode(width, height, true))
                    return false;
            }

            if (ShowWindow(platform->mainWindow, SW_RESTORE) == FALSE)
                return false;

            return true;
        }

    public:
        PlatformWin32()
            : mainWindow()
            , renderContext()
            , fullscreen()
        {
        }

        virtual bool CreateMainWindow() override
        {
            if (mainWindow) {
                // TODO: log message
                return false;
            }

            HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);

            WNDCLASS wndclass{};
            wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
            wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
            wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
            wndclass.hInstance = hInstance;
            wndclass.lpszClassName = MAIN_WINDOW_CLASS;
            wndclass.style = CS_HREDRAW | CS_VREDRAW;
            wndclass.lpfnWndProc = WindowProc;

            if (!RegisterClass(&wndclass)) {
                // TODO: fatal error
                return false;
            }

            if (CreateWindow(MAIN_WINDOW_CLASS, MAIN_WINDOW_TITLE,
                MAIN_WINDOW_STYLE, CW_USEDEFAULT, CW_USEDEFAULT,
                MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT, NULL, NULL, hInstance, NULL) == NULL) {
                //TODO: fatal error
                return false;
            }

            return true;
        }

        virtual void DestroyMainWindow() override
        {
            DestroyWindow(mainWindow);
        }

        virtual void SetMainWindowSize(int width, int height, bool setFullscreen) override
        {
            if (setFullscreen) {
                SetWindowLong(mainWindow, GWL_STYLE, WS_VISIBLE);
            }
            else {
                RECT clientArea{};
                clientArea.left = 0;
                clientArea.top = 0;
                clientArea.right = width;
                clientArea.bottom = height;

                if (AdjustWindowRect(&clientArea, MAIN_WINDOW_STYLE, FALSE) == FALSE) {
                    //TODO: log error
                    return;
                }

                width = clientArea.right - clientArea.left;
                height = clientArea.bottom - clientArea.top;
                SetWindowLong(mainWindow, GWL_STYLE, MAIN_WINDOW_STYLE);
            }

            if (!SetDisplayFullscreenMode(width, height, setFullscreen)) {
                //TODO: log warning
                return;
            }

            if (SetWindowPos(mainWindow, NULL, 0, 0, width, height, setFullscreen ? 0 : SWP_NOMOVE) == FALSE) {
                // TODO: log error
            }
        }

        virtual bool UpdateMainWindow() override
        {
            MSG msg;
            while (PeekMessage(&msg, mainWindow, 0, 0, PM_REMOVE)) {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }

            static const HDC deviceContext = GetDC(mainWindow);
            if (deviceContext)
                SwapBuffers(deviceContext);

            Sleep(1);
            return mainWindow != NULL;
        }
    
        FileHandle OpenFileRead(const String &path) override
        {
            const WString unicodePath = ConvertToUnicode(path);
            HANDLE hFile = CreateFileW(unicodePath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
            if (hFile == INVALID_HANDLE_VALUE) {
                //TODO: log message
                return INVALID_FILE_HANDLE;
            }

            return reinterpret_cast<FileHandle>(hFile);
        }

        FileHandle OpenFileWrite(const String &path) override
        {
            const WString unicodePath = ConvertToUnicode(path);
            const HANDLE hFile = CreateFileW(unicodePath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
            if (hFile == INVALID_HANDLE_VALUE) {
                //TODO: log message
                return INVALID_FILE_HANDLE;
            }

            return reinterpret_cast<FileHandle>(hFile);
        }

        size_t GetFilePointer(FileHandle handle, FileOrigin origin) override
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            const DWORD dwOrigin = ConvertToPlatformFileOrigin(origin);
            const DWORD result = ::SetFilePointer(hFile, 0, NULL, dwOrigin);
            if (result == INVALID_SET_FILE_POINTER) {
                return INVALID_FILE_POINTER;
            }

            return result;
        }

        size_t SetFilePointer(FileHandle handle, FileOrigin origin, size_t offset) override
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            const DWORD dwOrigin = ConvertToPlatformFileOrigin(origin);
            const DWORD result = ::SetFilePointer(hFile, offset, NULL, dwOrigin);
            if (result == INVALID_SET_FILE_POINTER) {
                return INVALID_FILE_POINTER;
            }

            return result;
        }

        bool ReadFile(FileHandle handle, Byte *data, size_t size) override
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            DWORD dwBytesRead;
            if (::ReadFile(hFile, data, size, &dwBytesRead, NULL) == FALSE) {
                return false;
            }

            if (dwBytesRead != size) {
                // TODO: log warning
            }
            
            return true;
        }

        bool WriteFile(FileHandle handle, const Byte *data, size_t size) override
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            DWORD dwBytesWritten;
            if (::WriteFile(hFile, data, size, &dwBytesWritten, NULL) == FALSE) {
                return false;
            }

            if (dwBytesWritten != size) {
                // TODO: log warning
            }

            return true;
        }

        bool IsEof(FileHandle handle)
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            const DWORD fileOffset = ::SetFilePointer(hFile, 0, NULL, FILE_CURRENT);
            if (fileOffset == INVALID_SET_FILE_POINTER)
                return true;

            const DWORD fileSize = ::GetFileSize(hFile, NULL);
            return fileOffset == fileSize;
        }

        size_t GetFileSize(FileHandle handle) override
        {
            const HANDLE hFile = reinterpret_cast<HANDLE>(handle);
            const DWORD fileSize = ::GetFileSize(hFile, NULL);
            if (fileSize == INVALID_FILE_SIZE)
                return INVALID_FILE_POINTER;

            return fileSize;
        }

        void CloseFile(FileHandle handle) override
        {
            CloseHandle(reinterpret_cast<HANDLE>(handle));
        }
    };

    Platform *GetPlatform()
    {
        static PlatformWin32 platformWin32;
        if (platform == nullptr)
            platform = &platformWin32;

        return platform;
    }
}
