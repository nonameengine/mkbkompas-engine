/*
*  File: math.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 19-11-2017
*
*  Copyright (c) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#define _USE_MATH_DEFINES

#include <assert.h>
#include <math.h>

namespace Engine
{
    class Vector3
    {
    public:
        Vector3() : x(), y(), z()
        {
        }

        Vector3(float x, float y, float z)
            : x(x), y(y), z(z)
        {
        }

        const float *GetData() const { return raw; }

        float GetX() const { return x; }
        float GetY() const { return y; }
        float GetZ() const { return z; }

        void SetZero() { *this = Vector3(); }

        void SetX(float value) { x = value; }
        void SetY(float value) { y = value; }
        void SetZ(float value) { z = value; }

        float &operator [](int index)
        {
            assert(index < 3);
            return raw[index];
        }

        float operator [](int index) const
        {
            assert(index < 3);
            return raw[index];
        }

    private:
        union
        {
            float raw[3];
            struct
            {
                float x, y, z;
            };
        };
    };

    class Vector4
    {
    public:
        Vector4() : x(), y(), z(), w()
        {
        }

        Vector4(const Vector3 &xyz, float w)
            : x(xyz.GetX())
            , y(xyz.GetY())
            , z(xyz.GetZ())
            , w(w)
        {
        }

        Vector4(float x, float y, float z, float w)
            : x(x), y(y), z(z), w(w)
        {
        }

        const float *GetData() const { return raw; }

        float GetX() const { return x; }
        float GetY() const { return y; }
        float GetZ() const { return z; }
        float GetW() const { return w; }

        void SetZero() { *this = Vector4(); }

        void SetX(float value) { x = value; }
        void SetY(float value) { y = value; }
        void SetZ(float value) { z = value; }
        void SetW(float value) { w = value; }

        float &operator [](int index)
        {
            assert(index < 4);
            return raw[index];
        }

        float operator [](int index) const
        {
            assert(index < 4);
            return raw[index];
        }

    private:
        union
        {
            float raw[4];
            struct
            {
                float x, y, z, w;
            };
        };
    };

    class Matrix3
    {
    public:
        Matrix3() : mat{}
        {
        }

        Matrix3(const Vector3 &x, const Vector3 &y, const Vector3 &z)
            : mat{x, y, z}
        {
        }

        Matrix3(float xx, float xy, float xz,
                float yx, float yy, float yz,
                float zx, float zy, float zz)
            : mat{Vector3(xx, xy, xz), Vector3(yz, yy, yz), Vector3(zx, zy, zz)}
        {
        }

        Matrix3(float yaw, float pitch, float roll) { SetRotation(yaw, pitch, roll); }

        const float *GetData() const { return reinterpret_cast<const float*>(&mat[0]); }

        const Vector3 &GetX() const { return mat[0]; }
        const Vector3 &GetY() const { return mat[1]; }
        const Vector3 &GetZ() const { return mat[2]; }

        void SetIdentity()
        {
            *this = Matrix3(
                1, 0, 0,
                0, 1, 0,
                0, 0, 1
            );
        }

        void SetZero() { *this = Matrix3(); }

        void SetX(const Vector3 &value) { mat[0] = value; }
        void SetY(const Vector3 &value) { mat[1] = value; }
        void SetZ(const Vector3 &value) { mat[2] = value; }

        void SetRotation(float yaw, float pitch, float roll)
        {
            const float sinYaw = sinf(yaw);
            const float sinPitch = sinf(pitch);
            const float sinRoll = sinf(roll);

            const float cosYaw = cosf(yaw);
            const float cosPitch = cosf(pitch);
            const float cosRoll = cosf(roll);

            mat[0][0] = cosYaw * cosRoll - sinYaw * sinPitch * sinRoll;
            mat[0][1] = cosYaw * sinRoll + sinYaw * sinPitch * cosRoll;
            mat[0][2] = -sinYaw * cosPitch;

            mat[1][0] = -cosPitch * sinRoll;
            mat[1][1] = cosPitch * cosRoll;
            mat[1][2] = sinPitch;

            mat[2][0] = sinYaw * cosRoll + cosYaw * sinPitch * sinRoll;
            mat[2][1] = sinYaw * sinRoll - cosYaw * sinPitch * cosRoll;
            mat[2][2] = cosYaw * cosPitch;
        }

        Vector3 &operator [](int index)
        {
            assert(index < 3);
            return mat[index];
        }

        const Vector3 &operator [](int index) const
        {
            assert(index < 3);
            return mat[index];
        }

    private:
        Vector3 mat[3];
    };

    class Matrix4
    {
    public:
        Matrix4() : mat{}
        {
        }

        Matrix4(const Matrix3 &matrix)
            : mat{Vector4(matrix.GetX(), 0),
                  Vector4(matrix.GetY(), 0),
                  Vector4(matrix.GetZ(), 0),
                  Vector4(0, 0, 0, 1)} 
        {
        }

        Matrix4(const Vector4 &x, const Vector4 &y, const Vector4 &z, const Vector4 &w)
            : mat{x, y, z, w}
        {
        }

        Matrix4(float xx, float xy, float xz, float xw,
                float yx, float yy, float yz, float yw,
                float zx, float zy, float zz, float zw,
                float wx, float wy, float wz, float ww)
            : mat{Vector4(xx, xy, xz, xw),
                  Vector4(yx, yy, yz, yw),
                  Vector4(zx, zy, zz, zw),
                  Vector4(wx, wy, wz, ww)}
        {
        }

        const float *GetData() const { return reinterpret_cast<const float*>(&mat[0]); }

        const Vector4 &GetX() const { return mat[0]; }
        const Vector4 &GetY() const { return mat[1]; }
        const Vector4 &GetZ() const { return mat[2]; }
        const Vector4 &GetW() const { return mat[3]; }

        void SetIdentity()
        {
            *this = Matrix4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            );
        }

        void SetZero() { *this = Matrix4(); }

        void SetX(const Vector4 &value) { mat[0] = value; }
        void SetY(const Vector4 &value) { mat[1] = value; }
        void SetZ(const Vector4 &value) { mat[2] = value; }
        void SetW(const Vector4 &value) { mat[3] = value; }

        void SetPerspective(float fov, float aspect, float znear, float zfar)
        {
            assert(aspect && fov > 0 && fov < M_PI && znear < zfar);

            const float m22 = 1.0f / tanf(fov / 2.0f);
            const float m11 = m22 / aspect;
            const float m33 = zfar / (znear - zfar);
            const float m34 = -1.0f;
            const float m43 = znear * m33;

            mat[0] = Vector4(m11, 0, 0, 0);
            mat[1] = Vector4(0, m22, 0, 0 );
            mat[2] = Vector4(0, 0, m33, m34);
            mat[3] = Vector4(0, 0, m43, 0);
        }

        void SetRotation(const Matrix3 &rotation)
        {
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    mat[i][j] = rotation[i][j];
                }
            }

            mat[3] = { 0, 0, 0, 1.0f };
        }

        void SetTranslation(const Vector3 &translation)
        {
            mat[0] = Vector4(1.0f, 0, 0, 0);
            mat[1] = Vector4(0, 1.0f, 0, 0);
            mat[2] = Vector4(0, 0, 1.0f, 0);
            mat[3] = Vector4(translation, 1.0f);
        }

        void SetScale(const Vector3 &scale)
        {
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    if (i == j)
                        mat[i][j] = scale[i];
                    else
                        mat[i][j] = 0;
                }
            }

            mat[3] = { 0, 0, 0, 1.0f };
        }

        Vector4 &operator [](int index)
        {
            assert(index < 4);
            return mat[index];
        }

        const Vector4 &operator [](int index) const
        {
            assert(index < 4);
            return mat[index];
        }

        Matrix4 operator * (const Matrix4 &rhs) const
        {
            Matrix4 temp;
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    for (int k = 0; k < 4; ++k) {
                        temp[i][j] += mat[i][k] * rhs[k][j];
                    }
                }
            }
            return temp;
        }

    private:
        Vector4 mat[4];
    };
}