/*
 *  File: renderer.cpp
 *
 *  Author: Ivan Shishkin <codingdude@gmail.com>
 *
 *  Date: 26-11-2017
 *
 *  Copyright (c) 2017 Ivan Shishkin All rights reserved
 */

#include <assert.h>

#include <array>
#include <unordered_map>

#include "renderer.h"

#ifdef _WIN32
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include "glext.h"

PFNGLGENBUFFERSPROC glGenBuffers;
PFNGLBINDBUFFERPROC glBindBuffer;
PFNGLBUFFERDATAPROC glBufferData;
PFNGLDELETEBUFFERSPROC glDeleteBuffers;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
PFNGLCREATESHADERPROC glCreateShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLDELETESHADERPROC glDeleteShader;
PFNGLCREATEPROGRAMPROC glCreateProgram;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLGETPROGRAMIVPROC glGetProgramiv;
PFNGLDELETEPROGRAMPROC glDeleteProgram;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM1IVPROC glUniform1iv;
PFNGLUNIFORM1FVPROC glUniform1fv;
PFNGLUNIFORM3FVPROC glUniform3fv;
PFNGLUNIFORM4FVPROC glUniform4fv;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D;
PFNGLACTIVETEXTUREPROC glActiveTexture;

static bool  ExposeOpenglInterface()
{
#define EXPOSEGLPROC(type, name) \
    name = (type) wglGetProcAddress(#name); \
    if (name == NULL) \
        return false; \

    EXPOSEGLPROC(PFNGLGENBUFFERSPROC, glGenBuffers)
    EXPOSEGLPROC(PFNGLBINDBUFFERPROC, glBindBuffer)
    EXPOSEGLPROC(PFNGLBUFFERDATAPROC, glBufferData)
    EXPOSEGLPROC(PFNGLDELETEBUFFERSPROC, glDeleteBuffers)
    EXPOSEGLPROC(PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer)
    EXPOSEGLPROC(PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray)
    EXPOSEGLPROC(PFNGLDISABLEVERTEXATTRIBARRAYPROC, glDisableVertexAttribArray)
    EXPOSEGLPROC(PFNGLCREATESHADERPROC, glCreateShader)
    EXPOSEGLPROC(PFNGLSHADERSOURCEPROC, glShaderSource)
    EXPOSEGLPROC(PFNGLCOMPILESHADERPROC, glCompileShader)
    EXPOSEGLPROC(PFNGLGETSHADERIVPROC, glGetShaderiv)
    EXPOSEGLPROC(PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog)
    EXPOSEGLPROC(PFNGLDELETESHADERPROC, glDeleteShader)
    EXPOSEGLPROC(PFNGLCREATEPROGRAMPROC, glCreateProgram)
    EXPOSEGLPROC(PFNGLATTACHSHADERPROC, glAttachShader)
    EXPOSEGLPROC(PFNGLBINDATTRIBLOCATIONPROC, glBindAttribLocation)
    EXPOSEGLPROC(PFNGLLINKPROGRAMPROC, glLinkProgram)
    EXPOSEGLPROC(PFNGLGETPROGRAMIVPROC, glGetProgramiv)
    EXPOSEGLPROC(PFNGLDELETEPROGRAMPROC, glDeleteProgram)
    EXPOSEGLPROC(PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog)
    EXPOSEGLPROC(PFNGLUSEPROGRAMPROC, glUseProgram)
    EXPOSEGLPROC(PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation)
    EXPOSEGLPROC(PFNGLUNIFORM1IPROC, glUniform1i)
    EXPOSEGLPROC(PFNGLUNIFORM1IVPROC, glUniform1iv)
    EXPOSEGLPROC(PFNGLUNIFORM1FVPROC, glUniform1fv)
    EXPOSEGLPROC(PFNGLUNIFORM4FVPROC, glUniform4fv);
    EXPOSEGLPROC(PFNGLUNIFORM3FVPROC, glUniform3fv);
    EXPOSEGLPROC(PFNGLUNIFORMMATRIX4FVPROC, glUniformMatrix4fv)
    EXPOSEGLPROC(PFNGLCOMPRESSEDTEXIMAGE2DPROC, glCompressedTexImage2D)
    EXPOSEGLPROC(PFNGLACTIVETEXTUREPROC, glActiveTexture)

#undef EXPOSEGLPROC

    return true;
}
#endif

namespace Engine
{
    struct HardwareBufferData
    {
        HardwareBufferData()
            : vertexFormat(INVALID_VERTEX)
            , vertexBufferId(0)
            , indexBufferId(0)
            , elementsCount(0)
        {
        }

        HardwareBufferData(VertexFormat vertexFormat, GLuint vertexBufferId, GLuint indexBufferId, GLsizei elementsCount)
            : vertexFormat(vertexFormat), vertexBufferId(vertexBufferId), indexBufferId(indexBufferId), elementsCount(elementsCount)
        {
        }

        VertexFormat vertexFormat;
        GLuint vertexBufferId;
        GLuint indexBufferId;
        GLsizei elementsCount;
    };

    struct GpuProgramData
    {
        GpuProgramData()
            : vertexFormat(INVALID_VERTEX)
            , vertexShaderId(0)
            , fragmentShaderId(0)
            , programId(0)
        {
        }

        GpuProgramData(VertexFormat vertexFormat, GLuint vertexShaderId, GLuint fragmentShaderId, GLuint programId)
            : vertexFormat(vertexFormat), vertexShaderId(vertexShaderId), fragmentShaderId(fragmentShaderId), programId(programId)
        {
        }

        VertexFormat vertexFormat;
        GLuint vertexShaderId;
        GLuint fragmentShaderId;
        GLuint programId;
    };

    struct HardwareTextureData
    {
        HardwareTextureData()
            : pixelFormat(PIXEL_FORMAT_INVALID)
            , target(GL_INVALID_ENUM)
            , hardwareTextureId(0)
        {
        }

        HardwareTextureData(PixelFormat pixelFormat, GLenum target, GLuint hardwareTextureId)
            : pixelFormat(pixelFormat), target(target), hardwareTextureId(hardwareTextureId)
        {
        }

        PixelFormat pixelFormat;
        GLenum target;
        GLuint hardwareTextureId;
    };

    struct VertexAttributeDescriptor
    {
        const char *name;
        VertexAttributes mask;
        GLuint index;
        GLint count;
        GLenum type;
    };

    struct PixelFormatDescriptor
    {
        PixelFormat format;
        GLint baseFormat;
        GLenum internalFormat;
        GLenum type;
        union
        {
            int bytesPerPixel;
            int bytesPerBlock;
        };
        bool compressed;
    };

    struct TextureMapTypeName
    {
        TextureMapType type;
        const char *name;
        GLenum internalName;
    };

    using HardwareBufferDataMap = std::unordered_map<HardwareBufferId, HardwareBufferData>;
    using GpuProgramDataMap = std::unordered_map<GpuProgramId, GpuProgramData>;
    using HardwareTextureDataMap = std::unordered_map<HardwareTextureId, HardwareTextureData>;

    constexpr std::array<VertexAttributeDescriptor, 10> vertexAttributeDescriptors = {{
        { "vertexPosition", VA_POINTS, 1, 3, GL_FLOAT },
        { "vertexNormals", VA_NORMALS, 2, 3, GL_FLOAT },
        { "vertexTangents", VA_TANGENTS, 3, 3, GL_FLOAT },
        { "vertexBinormals", VA_BINORMALS, 4, 3, GL_FLOAT },
        { "vertexTexcoords", VA_TEXCOORDS, 5, 2, GL_FLOAT },
        { "vertexWeights", VA_SKINNED_W1, 6, 1, GL_FLOAT },
        { "vertexWeights", VA_SKINNED_W2, 7, 2, GL_FLOAT },
        { "vertexWeights", VA_SKINNED_W3, 8, 3, GL_FLOAT },
        { "vertexWeights", VA_SKINNED_W4, 9, 4, GL_FLOAT },
        { "vertexBones", VA_BONES, 10, 1, GL_4_BYTES }
    }};

    constexpr std::array<PixelFormatDescriptor, 4> pixelFormatDescriptors = {{
        { PIXEL_FORMAT_RGBA, GL_RGBA, GL_RGBA8, GL_UNSIGNED_INT_8_8_8_8, 4, false },
        { PIXEL_FORMAT_DXT1, GL_RGBA, GL_COMPRESSED_RGBA_S3TC_DXT1_EXT, 0, 8, true },
        { PIXEL_FORMAT_DXT3, GL_RGBA, GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, 0, 16, true },
        { PIXEL_FORMAT_DXT5, GL_RGBA, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT, 0, 16, true }
    }};

    constexpr std::array<TextureMapTypeName, 3> textureMapTypeNames = {{
        { DIFFUSE_MAP, "diffuseMap", GL_TEXTURE0 },
        { NORMAL_MAP, "normalMap", GL_TEXTURE1 },
        { SPECULAR_MAP, "specularMap", GL_TEXTURE2 }
    }};

    static GLsizei GetVertexAttributeSize(const VertexAttributeDescriptor &desc)
    {
        GLsizei size = 0;

        switch (desc.type) {
        case GL_FLOAT:
            size = desc.count * sizeof(float);
            break;

        case GL_4_BYTES:
            size = desc.count * 4;
            break;

        default:
            assert(!"Unsupported type");
        }

        return size;
    }

    static GLsizei GetVertexSize(VertexFormat vertexFormat)
    {
        GLsizei size = 0;

        for (size_t i = 0; i < vertexAttributeDescriptors.size(); ++i) {
            const VertexAttributeDescriptor &attrib = vertexAttributeDescriptors[i];
            if (vertexFormat & attrib.mask) {
                size += GetVertexAttributeSize(attrib);
            }
        }

        return size;
    }

    static void SetVertexAttributesEnabled(VertexFormat vertexFormat, bool onoff)
    {
        GLchar *pointer = 0;
        const GLsizei stride = GetVertexSize(vertexFormat);

        for (size_t i = 0; i < vertexAttributeDescriptors.size(); ++i) {
            const VertexAttributeDescriptor &attrib = vertexAttributeDescriptors[i];
            if (vertexFormat & attrib.mask) {
                if (onoff) {
                    glVertexAttribPointer(attrib.index, attrib.count, attrib.type, GL_FALSE, stride, pointer);
                    glEnableVertexAttribArray(attrib.index);
                    pointer += GetVertexAttributeSize(attrib);
                }
                else {
                    glDisableVertexAttribArray(attrib.index);
                }
            }
        }
    }

    static const PixelFormatDescriptor *GetAvailablePixelFormatDescriptor(PixelFormat pixelFormat)
    {
        auto pixelFormatDescriptorIt = std::find_if(
            pixelFormatDescriptors.begin(),
            pixelFormatDescriptors.end(),
            [pixelFormat] (const PixelFormatDescriptor &pfd) {
                return pfd.format == pixelFormat;
            }
        );

        const PixelFormatDescriptor *pixelFormatDescriptor = nullptr;
        if (pixelFormatDescriptorIt != pixelFormatDescriptors.end())
            pixelFormatDescriptor = &(*pixelFormatDescriptorIt);

        return pixelFormatDescriptor;
    }

    static int GetPixelBlockSize(int width, int height, const PixelFormatDescriptor *pixelFormatDescriptor)
    {
        switch (pixelFormatDescriptor->format) {
        case PIXEL_FORMAT_RGBA:
            return width * height * pixelFormatDescriptor->bytesPerPixel;
        case PIXEL_FORMAT_DXT1:
        case PIXEL_FORMAT_DXT3:
        case PIXEL_FORMAT_DXT5: {
            const int pixelBlockSize = 4;
            width = (width + pixelBlockSize-1) / pixelBlockSize;
            height = (height + pixelBlockSize-1) / pixelBlockSize;
            return width * height * pixelFormatDescriptor->bytesPerBlock;
        }
        }
        return 0;
    }

    static GLuint CreateAndCompileShader(const ByteVector &shaderProgram, GLenum shaderType, String &shaderErrorLog)
    {
        GLint isCompilationSuccessfull;
        const GLuint shaderId = glCreateShader(shaderType);
        const GLchar *shaderSource = reinterpret_cast<const GLchar*>(shaderProgram.data());
        const GLint length = static_cast<GLsizei>(shaderProgram.size());
        glShaderSource(shaderId, 1, &shaderSource, &length);
        glCompileShader(shaderId);

        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &isCompilationSuccessfull);
        if (!isCompilationSuccessfull) {
            GLint length;
            glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

            shaderErrorLog.resize(length);
            glGetShaderInfoLog(shaderId, length, &length, &shaderErrorLog[0]);
            glDeleteShader(shaderId);

            return 0;
        }

        return shaderId;
    }

    static void BindVertexAttributes(VertexFormat vertexFormat, GLuint shaderProgramId)
    {
        for (size_t i = 0; i < vertexAttributeDescriptors.size(); ++i) {
            const VertexAttributeDescriptor &attrib = vertexAttributeDescriptors[i];
            if (vertexFormat & attrib.index) {
                glBindAttribLocation(shaderProgramId, attrib.index, attrib.name);
                if (glGetError() != GL_NO_ERROR) {
                    //TODO: log warning
                }
            }
        }
    }

    static void BindTextureMapTypes(GLuint shaderProgramId)
    {
        for (size_t i = 0; i < textureMapTypeNames.size(); ++i) {
            const TextureMapTypeName &typeName = textureMapTypeNames[i];
            GLint location = glGetUniformLocation(shaderProgramId, typeName.name);
            if (location != -1)
                glUniform1i(location, typeName.type);
        }
    }

    class RendererOpengl : public Renderer
    {
    public:
        RendererOpengl()
            : nextHardwareBufferId()
            , nextGpuProgramId()
            , nextHardwareTextureId()
        {
            if (!ExposeOpenglInterface()) {
                //TODO: log error message
                std::abort();
            }

            hardwareBuffers.reserve(MAX_HARDWARE_BUFFERS);
            gpuPrograms.reserve(MAX_GPU_PROGRAMS);
            hardwareTextures.reserve(MAX_HARDWARE_TEXTURES);

            glEnable(GL_DEPTH_TEST);
        }

        virtual void BeginRenderFrame() override
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }

        virtual void EndRenderFrame() override
        {
            // Do nothing
        }

        virtual void SetViewport(int width, int height) override
        {
            glViewport(0, 0, width, height);
        }

        virtual HardwareBufferId CreateHardwareBuffer(VertexFormat vertexFormat, const ByteVector &vertices, const ByteVector &indices) override
        {
            if (hardwareBuffers.size() >= MAX_HARDWARE_BUFFERS) {
                //TODO: show error message
                return INVALID_HARDWARE_BUFFER_ID;
            }

            GLuint vertexBufferId, indexBufferId;

            glGenBuffers(1, &vertexBufferId);
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
            glBufferData(GL_ARRAY_BUFFER, vertices.size(), vertices.data(), GL_STATIC_DRAW);

            glGenBuffers(1, &indexBufferId);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size(), indices.data(), GL_STATIC_DRAW);

            HardwareBufferId hardwareBufferId = ++nextHardwareBufferId;
            hardwareBuffers[hardwareBufferId] = HardwareBufferData(
                vertexFormat, vertexBufferId, indexBufferId, static_cast<GLsizei>(indices.size()) / 2);
            return hardwareBufferId;
        }

        virtual void DestroyHardwareBuffer(HardwareBufferId bufferId) override
        {
            HardwareBufferDataMap::iterator it = hardwareBuffers.find(bufferId);
            if (it != hardwareBuffers.end()) {
                const HardwareBufferData &bufferData = it->second;
                glDeleteBuffers(1, &bufferData.vertexBufferId);
                glDeleteBuffers(1, &bufferData.indexBufferId);
                hardwareBuffers.erase(it);
            }
        }

        virtual void DrawHardwareBuffer(HardwareBufferId bufferId) override
        {
            HardwareBufferDataMap::const_iterator it = hardwareBuffers.find(bufferId);
            if (it != hardwareBuffers.end()) {
                const HardwareBufferData &bufferData = it->second;
                glBindBuffer(GL_ARRAY_BUFFER, bufferData.vertexBufferId);
                SetVertexAttributesEnabled(bufferData.vertexFormat, true);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferData.indexBufferId);
                glDrawElements(GL_TRIANGLES, bufferData.elementsCount, GL_UNSIGNED_SHORT, NULL);
                SetVertexAttributesEnabled(bufferData.vertexFormat, false);
            }
        }

        virtual GpuProgramId CreateGpuProgram(VertexFormat vertexFormat, const ByteVector &vertexProgram, const ByteVector &fragmentProgram) override
        {
            if (gpuPrograms.size() >= MAX_GPU_PROGRAMS) {
                //TODO: log error message
                return INVALID_GPU_PROGRAM_ID;
            }

            std::string errorLog;
            const GLuint vertexShaderId = CreateAndCompileShader(vertexProgram, GL_VERTEX_SHADER, errorLog);
            if (vertexShaderId == 0) {
                //TODO: log error message
                return INVALID_GPU_PROGRAM_ID;
            }

            const GLuint fragmentShaderId = CreateAndCompileShader(fragmentProgram, GL_FRAGMENT_SHADER, errorLog);
            if (fragmentShaderId == 0) {
                //TODO: log error message
                glDeleteShader(vertexShaderId);
                return INVALID_GPU_PROGRAM_ID;
            }

            GLuint programId = glCreateProgram();
            glAttachShader(programId, vertexShaderId);
            glAttachShader(programId, fragmentShaderId);

            BindVertexAttributes(vertexFormat, programId);
            glLinkProgram(programId);
            
            GLint isProgramLinkedSuccessfully;
            glGetProgramiv(programId, GL_LINK_STATUS, &isProgramLinkedSuccessfully);
            if (!isProgramLinkedSuccessfully) {
                GLint length;
                glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &length);
                std::string errorLog(length, 0);
                glGetProgramInfoLog(programId, length, &length, &errorLog[0]);

                //TODO: log error message

                glDeleteShader(vertexShaderId);
                glDeleteShader(fragmentShaderId);
                glDeleteProgram(programId);

                return INVALID_GPU_PROGRAM_ID;
            }

            GpuProgramId gpuProgramId = ++nextGpuProgramId;
            gpuPrograms[gpuProgramId] = GpuProgramData(vertexFormat, vertexShaderId, fragmentShaderId, programId);
            
            return gpuProgramId;
        }

        virtual void DestroyGpuProgram(GpuProgramId programId) override
        {
            GpuProgramDataMap::iterator it = gpuPrograms.find(programId);
            if (it != gpuPrograms.end()) {
                const GpuProgramData &programData = it->second;
                glDeleteProgram(programData.programId);
                glDeleteShader(programData.fragmentShaderId);
                glDeleteShader(programData.vertexShaderId);
            }
        }

        virtual bool BindGpuProgram(GpuProgramId programId) override
        {
            GpuProgramDataMap::iterator it = gpuPrograms.find(programId);
            if (it != gpuPrograms.end()) {
                const GpuProgramData &programData = it->second;
                glUseProgram(programData.programId);
                if (glGetError() != GL_NO_ERROR) {
                    //TODO: error message
                    return false;
                }

                BindTextureMapTypes(programData.programId);
                return true;
            }

            return false;
        }

        virtual bool SetGpuProgramConstant(GpuProgramId programId, const String &name, const Matrix4 &matrix) override
        {
            return SetUniform(
                programId, name, 1,  matrix.GetData(),
                [](GLint location, GLsizei count, const float *data) {
                    glUniformMatrix4fv(location, count, GL_FALSE, data);
                }
            );
        }

        virtual HardwareTextureId CreateHardwareTexture2D(int width, int height, int mipmapLevel, PixelFormat pixelFormat, const ByteVector &data) override
        {
            const PixelFormatDescriptor *pixelFormatDescriptor = GetAvailablePixelFormatDescriptor(pixelFormat);
            if (pixelFormatDescriptor == nullptr) {
                //TODO: log error
                return INVALID_HARDWARE_TEXTURE_ID;
            }

            GLuint textureId;
            glGenTextures(1, &textureId);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, textureId);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipmapLevel - 1);

            const Byte *source = data.data();
            for (int i = 0; i < mipmapLevel; ++i) {
                int size = GetPixelBlockSize(width, height, pixelFormatDescriptor);
                if (size == 0) {
                    //TODO: log error
                    glDeleteTextures(1, &textureId);
                    return INVALID_HARDWARE_TEXTURE_ID;
                }

                if (pixelFormatDescriptor->compressed) {
                    glCompressedTexImage2D(GL_TEXTURE_2D, i, pixelFormatDescriptor->internalFormat, width, height, 0, size, source);
                }
                else {
                    glTexImage2D(GL_TEXTURE_2D, i, pixelFormatDescriptor->baseFormat, width, height, 0,
                        pixelFormatDescriptor->internalFormat, pixelFormatDescriptor->type, source);
                }

                if (glGetError() != GL_NO_ERROR) {
                    //TODO: log error
                    glDeleteTextures(1, &textureId);
                    return INVALID_HARDWARE_TEXTURE_ID;
                }

                width /= 2;
                height /= 2;
                source += size;
            }

            HardwareTextureId hardwareTextureId = ++nextHardwareTextureId;
            hardwareTextures[hardwareTextureId] = HardwareTextureData(pixelFormat, GL_TEXTURE_2D, hardwareTextureId);

            return hardwareTextureId;
        }

        virtual void DestroyHardwareTexture(HardwareTextureId textureId) override
        {
            auto hardwareTextureDataIt = hardwareTextures.find(textureId);
            if (hardwareTextureDataIt == hardwareTextures.end()) {
                //TODO: log error
                return;
            }

            glDeleteTextures(1, &hardwareTextureDataIt->second.hardwareTextureId);
            hardwareTextures.erase(hardwareTextureDataIt);
        }

        virtual bool BindHardwareTexture(HardwareTextureId textureId, TextureMapType textureMapType) override
        {
            auto hardwareTextureDataIt = hardwareTextures.find(textureId);
            if (hardwareTextureDataIt == hardwareTextures.end()) {
                //TODO: log error
                return false;
            }

            const auto textureMapTypeNameIt = std::find_if(
                textureMapTypeNames.begin(),
                textureMapTypeNames.end(),
                [textureMapType] (const TextureMapTypeName &typeName) {
                    return typeName.type == textureMapType;
                }
            );

            if (textureMapTypeNameIt == textureMapTypeNames.end()) {
                //TODO: log error
                return false;
            }

            glActiveTexture(textureMapTypeNameIt->internalName);
            glBindTexture(hardwareTextureDataIt->second.target, hardwareTextureDataIt->second.hardwareTextureId);
            if (glGetError() != GL_NO_ERROR) {
                //TODO: error message
                return false;
            }

            return true;
        }

    private:
        template <class ElementType, class UniformFunc>
        bool SetUniform(GpuProgramId programId, const String &name, GLsizei count, const ElementType *data, UniformFunc glUniform)
        {
            GpuProgramDataMap::iterator it = gpuPrograms.find(programId);
            if (it != gpuPrograms.end()) {
                const GpuProgramData &programData = it->second;
                GLint location = glGetUniformLocation(programData.programId, name.c_str());
                if (location == -1)
                    return false;

                glUniform(location, count, data);
                return true;
            }

            return false;
        }

    private:
        static const size_t MAX_HARDWARE_BUFFERS = 65532;
        static const size_t MAX_GPU_PROGRAMS = 1024;
        static const size_t MAX_HARDWARE_TEXTURES = 2048;

        HardwareBufferId nextHardwareBufferId;
        GpuProgramId nextGpuProgramId;
        HardwareTextureId nextHardwareTextureId;

        HardwareBufferDataMap hardwareBuffers;
        GpuProgramDataMap gpuPrograms;
        HardwareTextureDataMap hardwareTextures;
    };

    Renderer *GetRenderer()
    {
        static RendererOpengl renderer;
        return &renderer;
    }
}