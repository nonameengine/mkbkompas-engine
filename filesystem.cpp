/*
*  File: filesystem.cpp
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 07-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#include "filesystem.h"
#include "platform.h"

namespace Engine
{
    class FileImpl : public File
    {
    public:
        explicit FileImpl(FileHandle fileHandle)
            : fileHandle(fileHandle)
        {
        }

        ~FileImpl()
        {
            GetPlatform()->CloseFile(fileHandle);
        }

    protected:
        virtual void Read(Byte *data, int size) const
        {
            if (!GetPlatform()->ReadFile(fileHandle, data, size)) {
                //TODO: fatal error
                std::terminate();
            }
        }

        virtual void Write(const Byte *data, int size)
        {
            if (!GetPlatform()->WriteFile(fileHandle, data, size)) {
                //TODO: fatal error
                std::terminate();
            }
        }

        virtual bool IsEof() const override
        {
            return GetPlatform()->IsEof(fileHandle);
        }

        virtual size_t GetSize() const override
        {
            return GetPlatform()->GetFileSize(fileHandle);
        }

        virtual void Skip(size_t nbytes) override
        {
            GetPlatform()->SetFilePointer(fileHandle, FILE_ORIGIN_CURRENT, nbytes);
        }

    private:
        FileHandle fileHandle;
    };

    class FileSystemImpl : public FileSystem
    {
    public:
        virtual FilePtr OpenFileRead(const String &path) override
        {
            FileHandle fileHandle = GetPlatform()->OpenFileRead(path);
            if (fileHandle == INVALID_FILE_HANDLE) {
                //TODO: log warning
                return nullptr;
            }

            return std::make_shared<FileImpl>(fileHandle);
        }

        virtual FilePtr OpenFileWrite(const String &path) override
        {
            FileHandle fileHandle = GetPlatform()->OpenFileWrite(path);
            if (fileHandle == INVALID_FILE_HANDLE) {
                //TODO: log warning
                return nullptr;
            }

            return std::make_shared<FileImpl>(fileHandle);
        }
    };

    FileSystem *GetFileSystem()
    {
        static FileSystemImpl fileSystem;
        return &fileSystem;
    }
}