/*
*  File: types.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 26-11-2017
*
*  Copyright (c) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include <string>
#include <vector>

namespace Engine
{
    using Byte = unsigned char;
    using ByteVector = std::vector<Byte>;
    using String = std::string;
    using WString = std::wstring;
}