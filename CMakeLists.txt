cmake_minimum_required(VERSION 3.5)
project(engine)

# Compiler settings

set(
  ENGINE_SRC
  filesystem.cpp
  filesystem.h
  glext.h
  math.h
  model.cpp
  model.h
  platform.cpp
  platform.h
  renderer.cpp
  renderer.h
  texture.cpp
  texture.h
  types.h
)

# Linker settings

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/../lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set(CMAKE_DEBUG_POSTFIX d)

add_library(engine STATIC ${ENGINE_SRC})
  