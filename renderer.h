/*
*  File: renderer.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 26-11-2017
*
*  Copyright (c) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include "math.h"
#include "types.h"

namespace Engine
{
    enum VertexAttributes
    {
        VA_POINTS = 0x0001,
        VA_NORMALS = 0x0002,
        VA_TANGENTS = 0x0004,
        VA_BINORMALS = 0x0008,
        VA_TEXCOORDS = 0x0010,
        VA_SKINNED_W1 = 0x0020,
        VA_SKINNED_W2 = 0x0040,
        VA_SKINNED_W3 = 0x0080,
        VA_SKINNED_W4 = 0x0100,
        VA_BONES = 0x0200
    };

    enum VertexFormat
    {
        INVALID_VERTEX,
        STATIC_VERTEX = VA_POINTS | VA_NORMALS | VA_TANGENTS | VA_BINORMALS | VA_TEXCOORDS,
        SKINNED_VERTEX_1W = STATIC_VERTEX | VA_SKINNED_W1,
        SKINNED_VERTEX_2W = STATIC_VERTEX | VA_SKINNED_W2,
        SKINNED_VERTEX_3W = STATIC_VERTEX | VA_SKINNED_W3,
        SKINNED_VERTEX_4W = STATIC_VERTEX | VA_SKINNED_W4
    };

    enum PixelFormat
    {
        PIXEL_FORMAT_INVALID,
        PIXEL_FORMAT_RGBA,
        PIXEL_FORMAT_DXT1,
        PIXEL_FORMAT_DXT3,
        PIXEL_FORMAT_DXT5
    };

    enum TextureMapType
    {
        DIFFUSE_MAP,
        NORMAL_MAP,
        SPECULAR_MAP
    };

    using HardwareBufferId = int;
    using GpuProgramId = int;
    using HardwareTextureId = int;

    const HardwareBufferId INVALID_HARDWARE_BUFFER_ID = 0;
    const GpuProgramId INVALID_GPU_PROGRAM_ID = 0;
    const HardwareTextureId INVALID_HARDWARE_TEXTURE_ID = 0;

    class Renderer
    {
    public:
        virtual ~Renderer() {}

        virtual void BeginRenderFrame() = 0;
        virtual void EndRenderFrame() = 0;

        virtual void SetViewport(int width, int height) = 0;

        virtual HardwareBufferId CreateHardwareBuffer(VertexFormat vertexFormat, const ByteVector &vertices, const ByteVector &indices) = 0;
        virtual void DestroyHardwareBuffer(HardwareBufferId bufferId) = 0;
        virtual void DrawHardwareBuffer(HardwareBufferId bufferId) = 0;

        virtual GpuProgramId CreateGpuProgram(VertexFormat vertexFormat, const ByteVector &vertexProgram, const ByteVector &fragmentProgram) = 0;
        virtual void DestroyGpuProgram(GpuProgramId programId) = 0;
        virtual bool BindGpuProgram(GpuProgramId programId) = 0;
        virtual bool SetGpuProgramConstant(GpuProgramId programId, const String &name, const Matrix4 &matrix) = 0;

        virtual HardwareTextureId CreateHardwareTexture2D(int width, int height, int mipmapLevel, PixelFormat pixelFormat, const ByteVector &data) = 0;
        virtual void DestroyHardwareTexture(HardwareTextureId textureId) = 0;
        virtual bool BindHardwareTexture(HardwareTextureId textureId, TextureMapType textureMapType) = 0;
    };

    Renderer *GetRenderer();
}