/*
*  File: texture.cpp
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 17-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#include <unordered_map>

#include "filesystem.h"
#include "renderer.h"
#include "texture.h"

#define _LeastSignificant64(l) ((uint32_t)(((uint64_t)(l)) & 0xffffffff))
#define _MostSignificant64(l) ((uint32_t)((((uint64_t)(l)) >> 16) & 0xffffffff))

namespace Engine
{
    enum PvrPixelFormat
    {
        DXT1 = 7,
        DXT3 = 9,
        DXT5 = 11,
        RGBG8888 = 20
    };

    const char SUFFIX[] = ".pvr";
    const uint32_t VERSION = 0x03525650;

    PixelFormat GetInternalPixelFormat(uint32_t pixelFormat)
    {
        switch (pixelFormat) {
        case DXT1:
            return PIXEL_FORMAT_DXT1;
        case DXT3:
            return PIXEL_FORMAT_DXT3;
        case DXT5:
            return PIXEL_FORMAT_DXT5;
        case RGBG8888:
            return PIXEL_FORMAT_RGBA;
        }
        return PIXEL_FORMAT_INVALID;
    }

    class TextureImpl : public Texture
    {
    public:
        TextureImpl(const String &name)
            : name(name)
            , isLoaded(false)
            , hardwareTextureId(INVALID_HARDWARE_TEXTURE_ID)
        {
        }

        ~TextureImpl()
        {
            if (isLoaded)
                Unload();
        }

        virtual void Load() override
        {
            if (isLoaded) {
                //TODO: log warning
                return;
            }

            FilePtr file = GetFileSystem()->OpenFileRead(name + SUFFIX);
            if (!file) {
                //TODO: log error
                return;
            }

            const uint32_t version = file->ReadUInt32();
            if (version != VERSION) {
                //TODO: log error
                return;
            }

            const uint32_t flags = file->ReadUInt32();
            const uint64_t pixelFormat = file->ReadUInt64();
            const uint32_t colourSpace = file->ReadUInt32();
            const uint32_t channelType = file->ReadUInt32();
            const uint32_t height = file->ReadUInt32();
            const uint32_t width = file->ReadUInt32();
            const uint32_t depth = file->ReadUInt32();
            const uint32_t surfacesCount = file->ReadUInt32();
            const uint32_t facesCount = file->ReadUInt32();
            const uint32_t mipmapLevel = file->ReadUInt32();
            const uint32_t metadataSize = file->ReadUInt32();

            if (_MostSignificant64(pixelFormat) != 0
                || depth != 1
                || surfacesCount != 1
                || facesCount != 1) {
                //FIXME: not implemented
                std::terminate();
            }

            PixelFormat internalPixelFormat = GetInternalPixelFormat(_LeastSignificant64(pixelFormat));
            if (internalPixelFormat == PIXEL_FORMAT_INVALID) {
                //TODO: log error
                return;
            }

            if (metadataSize)
                file->Skip(metadataSize);

            const size_t headerSize = 52;
            const size_t fileSize = file->GetSize();
            const size_t dataSize = fileSize - headerSize - metadataSize;

            ByteVector textureData(dataSize);
            file->Read(&textureData[0], dataSize);

            hardwareTextureId = GetRenderer()->CreateHardwareTexture2D(width, height, mipmapLevel, internalPixelFormat, textureData);
            if (hardwareTextureId == INVALID_HARDWARE_TEXTURE_ID) {
                //TODO: log error
                return;
            }

            isLoaded = true;
        }

        virtual void Unload() override
        {
            if (!isLoaded) {
                //TODO: log warning
                return;
            }

            GetRenderer()->DestroyHardwareTexture(hardwareTextureId);
            hardwareTextureId = INVALID_HARDWARE_TEXTURE_ID;
            isLoaded = false;
        }

        virtual bool IsLoaded() const override
        {
            return isLoaded;
        }

        virtual HardwareTextureId GetHardwareTexture() const override
        {
            return hardwareTextureId;
        }

    private:
        String name;
        bool isLoaded;
        HardwareTextureId hardwareTextureId;
    };

    using TextureMap = std::unordered_map<String, TexturePtr>;

    class TextureManagerImpl : public TextureManager
    {
    public:
        TextureManagerImpl()
        {
            textureCache.reserve(MAX_TEXTURES);
        }

        virtual TexturePtr GetTexture(const String &name) override
        {
            TextureMap::const_iterator it = textureCache.find(name);
            if (it != textureCache.end())
                return it->second;

            if (textureCache.size() >= MAX_TEXTURES) {
                //TODO: log error
                return nullptr;
            }

            TexturePtr texture = std::make_shared<TextureImpl>(name);
            textureCache[name] = texture;
            texture->Load();

            return texture;
        }

    private:
        static const size_t MAX_TEXTURES = 2048;

        TextureMap textureCache;
    };

    TextureManager *GetTextureManager()
    {
        static TextureManagerImpl textureManager;
        return &textureManager;
    }
}