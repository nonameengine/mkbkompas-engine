/*
*  File: model.cpp
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 09-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#include <unordered_map>

#include "filesystem.h"
#include "math.h"
#include "model.h"
#include "renderer.h"
#include "types.h"

namespace Engine
{
    enum ModelType
    {
        STATIC_MODEL,
        SKINNED_MODEL
    };

    enum FileChunks
    {
        GRAPHICS_SHAPE_CHUNK_ID = 0x00000001,
    };

    const char SUFFIX[] = ".model";
    const char MAGIC[] = "mdl";

    const uint16_t VERSION_MAJOR = 0;
    const uint16_t VERSION_MINOR = 1;

    class ModelImpl : public Model
    {
    public:
        ModelImpl(const String &modelName)
            : name(modelName)
            , isLoaded(false)
        {
        }

        ~ModelImpl()
        {
            if (isLoaded)
                Unload();
        }

        void Load() override
        {
            if (isLoaded) {
                //TODO: log warning
                return;
            }

            const String fileName = name + SUFFIX;
            const FilePtr file = GetFileSystem()->OpenFileRead(fileName);
            if (!file) {
                //TODO: log warning
                return;
            }

            const String magic = file->ReadZeroTerminatedString();
            if (magic != MAGIC) {
                //TODO: log warning
                return;
            }

            const uint16_t versionMajor = file->ReadUInt16();
            const uint16_t versionMinor = file->ReadUInt16();
            if (versionMajor != VERSION_MAJOR || versionMinor != VERSION_MINOR) {
                //TODO: log warning
                return;
            }

            while (!file->IsEof()) {
                const uint32_t chunkId = file->ReadUInt32();
                switch (chunkId) {
                case GRAPHICS_SHAPE_CHUNK_ID:
                    ParseGraphicsShape(file);
                    break;
                }
            }

            isLoaded = true;
        }

        void Unload() override
        {
            if (!isLoaded) {
                //TODO: log warning
                return;
            }

            for (const auto &graphicsShape : graphicsShapes) {
                GetRenderer()->DestroyHardwareBuffer(graphicsShape->hardwareBufferId);
            }

            graphicsShapes.clear();
            isLoaded = false;
        }

        bool IsLoaded() const override
        {
            return isLoaded;
        }

        GraphicsShapeList GetGraphicsShapes() const
        {
            return graphicsShapes;
        }

    private:
        void ParseGraphicsShape(const FilePtr &file)
        {
            const uint32_t indexBufferSize = file->ReadUInt32();
            ByteVector indexBuffer(indexBufferSize);
            file->Read(&indexBuffer[0], indexBufferSize);

            const uint32_t vertexFormat = file->ReadInt32();
            const uint32_t vertexBufferSize = file->ReadUInt32();
            ByteVector vertexBuffer(vertexBufferSize);
            file->Read(&vertexBuffer[0], vertexBufferSize);

            GraphicsShapePtr graphicsShape = std::make_shared<GraphicsShape>();
            graphicsShape->hardwareBufferId = GetRenderer()->CreateHardwareBuffer(
                static_cast<VertexFormat>(vertexFormat), vertexBuffer, indexBuffer);

            graphicsShapes.push_back(graphicsShape);
        }

    private:
        String name;
        bool isLoaded;
        GraphicsShapeList graphicsShapes;
    };

    using ModelMap = std::unordered_map<String, ModelPtr>;

    class ModelManagerImpl : public ModelManager
    {
    public:
        ModelManagerImpl()
        {
            models.reserve(MAX_MODELS);
        }

        virtual ModelPtr GetModel(const String &modelName) override
        {
            ModelMap::const_iterator it = models.find(modelName);
            if (it != models.end())
                return it->second;

            if (models.size() >= MAX_MODELS) {
                //TODO: log error
                return nullptr;
            }

            ModelPtr model = std::make_shared<ModelImpl>(modelName);
            model->Load();
            models[modelName] = model;
            return model;
        }

    private:
        static const size_t MAX_MODELS = 0xffff;
        ModelMap models;
    };

    ModelManager *GetModelManager()
    {
        static ModelManagerImpl modelManager;
        return &modelManager;
    }
}