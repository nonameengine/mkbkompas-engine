/*
*  File: texture.h
*
*  Author: Ivan Shishkin <codingdude@gmail.com>
*
*  Date: 17-12-2017
*
*  Copyright (C) 2017 Ivan Shishkin All rights reserved
*/

#pragma once

#include <memory>

#include "renderer.h"
#include "types.h"

namespace Engine
{
    class Texture
    {
    public:
        virtual ~Texture() {}

        virtual void Load() = 0;
        virtual void Unload() = 0;
        virtual bool IsLoaded() const = 0;

        virtual HardwareTextureId GetHardwareTexture() const = 0;
    };

    using TexturePtr = std::shared_ptr<Texture>;

    class TextureManager
    {
    public:
        virtual ~TextureManager() {}

        virtual TexturePtr GetTexture(const String &name) = 0;
    };

    TextureManager *GetTextureManager();
}